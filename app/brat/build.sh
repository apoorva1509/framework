#!/bin/bash

cd "$(dirname "$0")"

# Generic config
version="0.1.0"
name="app_brat"
repo="cdli/framework"
registry="registry.gitlab.com"
build_date="$(date -u +"%Y-%m-%dT%H:%M:%SZ")"

# App-specific config
apk_repo="https://php.codecasts.rocks/v3.7/php-7.2"
php_version="$(\
  curl -s ${apk_repo}/x86_64/APKINDEX.tar.gz | \
  tar -xO APKINDEX | \
  awk '/P:php.-fpm/ { getline; gsub(/^V:|\-r[0-9]+$/, "", $0); print $0}' - | \
  sort --human-numeric-sort | tail -n 1)"

# Build command
docker build --force-rm --pull \
  --build-arg version="${version}" \
  --build-arg apk_repo="${apk_repo}" \
  --build-arg php_version="${php_version}" \
  --label build-date="${build_date}" \
  --tag "${registry}/${repo}/${name}:${version}" \
  --tag "${repo}/${name}:${version}" \
  .
