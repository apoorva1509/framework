<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Database\Schema;

/**
 * Stats Controller
 */

class StatsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Artifacts');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($table = 'artifacts')
    {
        $collection = $this->getCollection();
        
        // Get list of all the tables
        $tables = $collection->listTables();
        
        // Get Data to be displayed in the table
        $data = $this->getDisplayDataByTableName($table);
        
        // Get number of rows in the table
        $numRows = $this->Artifacts->getNumberOfRowsInTable($table);
        
        $this->set('selectedTable', $table);
        $this->set('tables', $tables);
        $this->set('data', $data);
        $this->set('numberOfRows', $numRows);
    }
    
    public function getCollection()
    {
        $db = ConnectionManager::get('default');

        // Create a schema collection
        $collection = $db->schemaCollection();
        
        return $collection;
    }
    
    public function getDisplayDataByTableName($table)
    {
        $collection = $this->getCollection();
        
        // Get artifacts table
        $tableSchema = $collection->describe($table);
        
        // Get columns list from table
        $columns = $tableSchema->columns();
        
        // Empty array for fields
        $data = [];
        
        // Iterate columns
        foreach ($columns as $column) {
            $data[$column] = $tableSchema->column($column);
            $data[$column]['nullValues'] = $this->Artifacts->getNumberOfNullValuesByColumn($table, $column);
            $data[$column]['distinctValues'] = $this->Artifacts->getNumberOfDistinctValuesByColumn($table, $column);
            $data[$column]['maxValue'] = $this->Artifacts->getMaximumValueByColumn($table, $column, $data[$column]['type']);
            $data[$column]['minValue'] = $this->Artifacts->getMinimumValueByColumn($table, $column, $data[$column]['type']);
        }
        
        return $data;
    }
}
