<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StaffType $staffType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Staff Type'), ['action' => 'edit', $staffType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Staff Type'), ['action' => 'delete', $staffType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $staffType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Staff Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Staff Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Staff'), ['controller' => 'Staff', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Staff'), ['controller' => 'Staff', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="staffTypes view large-9 medium-8 columns content">
    <h3><?= h($staffType->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Staff Type') ?></th>
            <td><?= h($staffType->staff_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($staffType->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Staff') ?></h4>
        <?php if (!empty($staffType->staff)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Author Id') ?></th>
                <th scope="col"><?= __('Staff Type Id') ?></th>
                <th scope="col"><?= __('Cdli Title') ?></th>
                <th scope="col"><?= __('Contribution') ?></th>
                <th scope="col"><?= __('Sequence') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($staffType->staff as $staff): ?>
            <tr>
                <td><?= h($staff->id) ?></td>
                <td><?= h($staff->author_id) ?></td>
                <td><?= h($staff->staff_type_id) ?></td>
                <td><?= h($staff->cdli_title) ?></td>
                <td><?= h($staff->contribution) ?></td>
                <td><?= h($staff->sequence) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Staff', 'action' => 'view', $staff->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Staff', 'action' => 'edit', $staff->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Staff', 'action' => 'delete', $staff->id], ['confirm' => __('Are you sure you want to delete # {0}?', $staff->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
