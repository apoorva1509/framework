<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation[]|\Cake\Collection\CollectionInterface $abbreviations
 */
?>

<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Publication'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Abbreviations'), ['controller' => 'Abbreviations', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Abbreviation'), ['controller' => 'Abbreviations', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Abbreviations') ?></h3>

<div>
    <div>
        <table class="table table-hover" cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
            <thead  align="left">
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('abb') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('meaning') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('publication_id') ?></th>
                    <th class="actions" scope="col"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($abbreviations as $abbreviation): ?>
                    <tr>
                        <td align="left"><a href="/admin/abbreviations/<?=h($abbreviation->id)?>"><?= h($abbreviation->abbreviation) ?></a></td>
                        <td align="left"><?= h($abbreviation->fullform) ?></td>
                        <td align="left"><?= h($abbreviation->type) ?></td>
                        <?php if(!empty($abbreviation->publications)):?>
                            <td align="left"><a href="publications/<?=h($abbreviation->publication_id)?>"><?= h($abbreviation->publications[0]->designation)?></a></td> 
                        <?php else:?>
                            <td></td>
                        <?php endif;?>
                        <td class="actions">
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $abbreviation->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $abbreviation->id], ['confirm' => __('Are you sure you want to delete # {0}?', $abbreviation->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
</div>
