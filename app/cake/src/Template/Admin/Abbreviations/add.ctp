<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation $abbreviation
 */
?>
<div class="row justify-content-md-center">
 
    <div class="col-lg-7 boxed">
        <div class="abbreviations form content">
            <?= $this->Form->create($abbreviation)?>
            <fieldset>
                <legend><?= __('Add Abbreviation') ?></legend>
                <?php
                    echo $this->Form->control('abbreviation');
                    echo $this->Form->control('fullform');
                    echo $this->Form->control('publication_id');
                    echo $this->Form->control('type');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Publications'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Abbreviations'), ['controller' => 'Abbreviations', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('New Abbreviation'), ['controller' => 'Abbreviations', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        
    </div>
</div>
