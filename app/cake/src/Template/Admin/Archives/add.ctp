<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Archive $archive
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($archive) ?>
            <legend class="capital-heading"><?= __('Add Archive') ?></legend>
            <?php
                echo $this->Form->control('archive');
                echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Archives'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Proveniences'), ['controller' => 'Proveniences', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Provenience'), ['controller' => 'Proveniences', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
