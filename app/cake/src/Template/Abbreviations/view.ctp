<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Abbreviation $abbreviation
 */

$this->assign('description', $abbreviation->fullform);
?>
<div class="row justify-content-md-center">
    <div class="col-lg-7 boxed">
        <div class="abbreviations view content">
            <div class="capital-heading"><?= h($abbreviation->abbreviation) ?></div>
            <table class="table-bootstrap">
                <tr>
                    <th><?= __('Abb') ?></th>
                    <td><?= h($abbreviation->abbreviation) ?></td>
                </tr>
                <tr>
                    <th><?= __('Meaning') ?></th>
                    <td><?= h($abbreviation->fullform) ?></td>
                </tr>


            </table>
            <div class="text">
                <strong><?= __('Type') ?></strong>
                <blockquote>
                    <?= h($abbreviation->type) ?>
                </blockquote>
            </div>

        </div>
    </div>
</div>

<div>
    <div>
        <div>
            <div class="boxed mx-0">
                <h4><?= __('Related Publications') ?></h4>
                <?php if (!empty($abbreviation->publications)) : ?>
                <div cellpadding="0" cellspacing="0" class="table-bootstrap">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Bibtexkey') ?></th>
                            <th><?= __('Year') ?></th>
                            <th><?= __('Entry Type Id') ?></th>
                            <th><?= __('Address') ?></th>
                            <th><?= __('Annote') ?></th>
                            <th><?= __('Book Title') ?></th>
                            <th><?= __('Chapter') ?></th>
                            <th><?= __('Crossref') ?></th>
                            <th><?= __('Edition') ?></th>
                            <th><?= __('Editor') ?></th>
                            <th><?= __('How Published') ?></th>
                            <th><?= __('Institution') ?></th>
                            <th><?= __('Journal Id') ?></th>
                            <th><?= __('Month') ?></th>
                            <th><?= __('Note') ?></th>
                            <th><?= __('Number') ?></th>
                            <th><?= __('Organization') ?></th>
                            <th><?= __('Pages') ?></th>
                            <th><?= __('Publisher') ?></th>
                            <th><?= __('School') ?></th>
                            <th><?= __('Title') ?></th>
                            <th><?= __('Volume') ?></th>
                            <th><?= __('Publication History') ?></th>
                            <th><?= __('Abbreviation Id') ?></th>
                            <th><?= __('Series') ?></th>
                            <th><?= __('Oclc') ?></th>
                            <th><?= __('Designation') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($abbreviation->publications as $publications) : ?>
                        <tr>
                            <td><?= h($publications->id) ?></td>
                            <td><?= h($publications->bibtexkey) ?></td>
                            <td><?= h($publications->year) ?></td>
                            <td><?= h($publications->entry_type_id) ?></td>
                            <td><?= h($publications->address) ?></td>
                            <td><?= h($publications->annote) ?></td>
                            <td><?= h($publications->book_title) ?></td>
                            <td><?= h($publications->chapter) ?></td>
                            <td><?= h($publications->crossref) ?></td>
                            <td><?= h($publications->edition) ?></td>
                            <td><?= h($publications->editor) ?></td>
                            <td><?= h($publications->how_published) ?></td>
                            <td><?= h($publications->institution) ?></td>
                            <td><?= h($publications->journal_id) ?></td>
                            <td><?= h($publications->month) ?></td>
                            <td><?= h($publications->note) ?></td>
                            <td><?= h($publications->number) ?></td>
                            <td><?= h($publications->organization) ?></td>
                            <td><?= h($publications->pages) ?></td>
                            <td><?= h($publications->publisher) ?></td>
                            <td><?= h($publications->school) ?></td>
                            <td><?= h($publications->title) ?></td>
                            <td><?= h($publications->volume) ?></td>
                            <td><?= h($publications->publication_history) ?></td>
                            <td><?= h($publications->abbreviation_id) ?></td>
                            <td><?= h($publications->series) ?></td>
                            <td><?= h($publications->oclc) ?></td>
                            <td><?= h($publications->designation) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
