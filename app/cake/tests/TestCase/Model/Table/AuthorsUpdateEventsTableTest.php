<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuthorsUpdateEventsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuthorsUpdateEventsTable Test Case
 */
class AuthorsUpdateEventsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AuthorsUpdateEventsTable
     */
    public $AuthorsUpdateEvents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.authors_update_events',
        'app.update_events',
        'app.authors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('AuthorsUpdateEvents') ? [] : ['className' => AuthorsUpdateEventsTable::class];
        $this->AuthorsUpdateEvents = TableRegistry::getTableLocator()->get('AuthorsUpdateEvents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AuthorsUpdateEvents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
