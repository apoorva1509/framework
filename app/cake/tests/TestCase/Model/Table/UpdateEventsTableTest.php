<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UpdateEventsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UpdateEventsTable Test Case
 */
class UpdateEventsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UpdateEventsTable
     */
    public $UpdateEvents;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.update_events',
        'app.authors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UpdateEvents') ? [] : ['className' => UpdateEventsTable::class];
        $this->UpdateEvents = TableRegistry::getTableLocator()->get('UpdateEvents', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UpdateEvents);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
